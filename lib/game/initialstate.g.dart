// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'initialstate.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InitialState _$InitialStateFromJson(Map<String, dynamic> json) {
  return InitialState(
    (json['tiles'] as List)
        ?.map(
            (e) => e == null ? null : Tile.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$InitialStateToJson(InitialState instance) =>
    <String, dynamic>{
      'tiles': instance.tiles?.map((e) => e?.toJson())?.toList(),
    };
