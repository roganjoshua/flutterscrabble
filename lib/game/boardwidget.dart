import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'dart:convert';
import 'board.dart';

class BoardWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        constraints: BoxConstraints(minWidth: 100, maxWidth: 800),
        child: new FutureBuilder(
            future:
                DefaultAssetBundle.of(context).loadString("assets/board.json"),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return GridView.count(
                    crossAxisCount: 15,
                    shrinkWrap: true,
                    children: getBoardFromJSON(snapshot.data.toString()));
              }
              return CircularProgressIndicator();
            }));
  }

  List<Widget> getBoardFromJSON(String data) {
    Map tileMap = jsonDecode(data);
    var board = Board.fromJson(tileMap);
    var rows = board.rows;
    var boardWidgets = new List<Widget>();
    var color = Colors.yellow;
    var text = '';
    rows.forEach((element) {
      element.forEach((element) {
        switch (element) {
          case "NO":
            color = Colors.yellow;
            text = ' ';
            break;
          case "TW":
            color = Colors.red;
            text = 'Triple\nWord';
            break;
          case "DL":
            color = Colors.lightBlue;
            text = 'Double\nLetter';
            break;
          case "DW":
            color = Colors.blue;
            text = 'Double\nWord';
            break;
          case "TL":
            color = Colors.blueGrey;
            text = 'Triple\nLetter';
            break;
          default:
            break;
        }
        boardWidgets.add(new FlatButton(
            //shadowColor: Colors.green,
            color: color,
            child: Center(
              child: FittedBox(
                  fit: BoxFit.fitWidth,
                  child: Text(
                    text,
                    textAlign: TextAlign.center,
                  )),
            )));
      });
    });
    return boardWidgets;
  }
}
