import 'package:json_annotation/json_annotation.dart';
part 'board.g.dart';
@JsonSerializable()

class Board
{
  List<List> rows;
  Board(this.rows);

  factory Board.fromJson(Map<String, dynamic> json) => _$BoardFromJson(json);
  Map<String, dynamic> toJson() => _$BoardToJson(this);
}