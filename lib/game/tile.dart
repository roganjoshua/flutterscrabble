import 'package:json_annotation/json_annotation.dart';
part 'tile.g.dart';
@JsonSerializable(explicitToJson: true)

class Tile{
   final String name;
   final int points;
   final int initialNumber;

  Tile(this.name,this.points,this.initialNumber);

  factory Tile.fromJson(Map<String, dynamic> json) => _$TileFromJson(json);
  Map<String, dynamic> toJson() => _$TileToJson(this);
}