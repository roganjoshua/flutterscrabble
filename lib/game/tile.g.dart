// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Tile _$TileFromJson(Map<String, dynamic> json) {
  return Tile(
    json['name'] as String,
    json['points'] as int,
    json['initialNumber'] as int,
  );
}

Map<String, dynamic> _$TileToJson(Tile instance) => <String, dynamic>{
      'name': instance.name,
      'points': instance.points,
      'initialNumber': instance.initialNumber,
    };
