// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'board.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Board _$BoardFromJson(Map<String, dynamic> json) {
  return Board(
    (json['rows'] as List)?.map((e) => e as List)?.toList(),
  );
}

Map<String, dynamic> _$BoardToJson(Board instance) => <String, dynamic>{
      'rows': instance.rows,
    };
