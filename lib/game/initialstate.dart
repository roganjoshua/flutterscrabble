import 'package:flutterscrabble/game/board.dart';
import 'package:flutterscrabble/game/tile.dart';
import 'package:json_annotation/json_annotation.dart';

part 'initialstate.g.dart';

@JsonSerializable(explicitToJson: true)

class InitialState
{

  List<Tile> tiles;
  InitialState(this.tiles);

  factory InitialState.fromJson(Map<String, dynamic> json) => _$InitialStateFromJson(json);
  Map<String, dynamic> toJson() => _$InitialStateToJson(this);

}